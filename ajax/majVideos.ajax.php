<?php
include ("../includes/params.php");
include("../includes/fonctions.php");
$nbrOld = 0;
$tabInst = chargerInstances();
$nbrInst = count($tabInst);
$idInst = 0;
$total = 0;

if(isset($_GET['choix']))
{
	//trouver l'ancien nombre de vidéos pour l'instance
	$instance = htmlentities($_GET['choix']);
	$sql = "SELECT `IDInst`,`NbrVideo`,`URLInst` FROM `Instances` WHERE `NomInst` = '$instance';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		$idInst = $result[0]['IDInst'];
		$nbrOld = $result[0]['NbrVideo'];
	   $urlInst = 'https://'.$result[0]['URLInst'];
	//trouver le nouveau nombre de vidéos pour l'instance
		$json = testInstancePeertube($urlInst);
		$tab = json_decode($json,true);
		$total = $tab['total'];
		debogue('maj','TOTAL',$total);
	}	
	//Afficher le nouveau formulaire
}

?>
   <h3>Mettre à jour les vidéos manuellement</h3>
	<form action='index.php' method='POST'>
		<label>Instance Peertube</label><br>
		 <select class='w3-select' name='refInst' style='width:98%' id='idRefInst' onchange=' majVideos();' >
		 <?php 
		 	echo "<option value='".$idInst."'>".$instance."</option>"; 
	 		for ($i = 0;$i < $nbrInst;$i++) 
		 		{
		 			if(strcmp($tabInst[$i]['NomInst'] ,$instance)) 
		 			{
				 		echo "<option value='".$tabInst[$i]['IDInst']."'>".$tabInst[$i]['NomInst']."</option>";
				 	} 
				}
		 	?>
		 </select><br><br>	   	    
		<label>Vidéos enregistrées </label>
 		<input type='number' name='video1' value='<?php echo $nbrOld; ?>'><br>
 		<label>Vidéos de l'instance </label>
 		<input type='number' name ='video2' value ='<?php echo $total; ?>'><br><br>
 		<input type='submit' name='maj'  value = 'Mettre à jour' 
 		class='w3-button w3-hover-light-blue w3-text-white btnbleu'>
    </form>	