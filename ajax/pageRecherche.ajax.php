<?php
session_start();
include_once("../includes/fonctions.php");

$idCat = 0;
$motCle =NULL;
$parution = 0;
$idChaine = 0;
$idInst = 0;
$debut = NULL;
$fin = NULL;
$tpsInf = NULL;
$tpsSup = NULL;
$idLangue = 0;
$idLicence = 0;
$sure = NULL;
$tri = 1;

if (isset($_SESSION['Instance'])) {
	$idInst = $_SESSION['Instance'];
}
if (isset($_SESSION['mot-cle'])) {
	$motCle = $_SESSION['mot-cle'];
}
if (isset($_SESSION['catego'])) {
	$idCat = $_SESSION['catego'];
}
if (isset($_SESSION['parution'])) {
	$parution = $_SESSION['parution'];
}
if (isset($_SESSION['chaine'])) {
	$idChaine = $_SESSION['chaine'];
}
if (isset($_SESSION['debut']) && isset($_SESSION['fin'])) {
	$debut = $_SESSION['debut'];
	$fin = $_SESSION['fin'];
	echo $debut;
	echo $fin;
}
if (isset($_SESSION['tpsInf']) && isset($_SESSION['tpsSup'])) {
	$tpsInf = $_SESSION['tpsInf'];
	$tpsSup = $_SESSION['tpsSup'];
}
if (isset($_SESSION['langue'])) {
	$idLangue = $_SESSION['langue'];
}
if (isset($_SESSION['licence'])) {
	$idLicence = $_SESSION['licence'];
}
if (isset($_SESSION['tri'])) {
	$tri = $_SESSION['tri'];
}
if (isset($_SESSION['SFW'])) {
	$sure = $_SESSION['SFW'];
}

if(isset($_GET['pageOld']) || isset($_GET['page']))
{
   $pageOld = $_GET['pageOld'];
   $page = $_GET['page'];
   afficherRecherche($pageOld,$page, $idCat,$motCle,$parution, $idChaine, $idInst, 
		 								$debut, $fin, $tpsInf, $tpsSup, $idLangue,
										$idLicence, $tri, $sure);    
}
else 
{
	echo "Erreur : la page ne peut pas être affichée";
}