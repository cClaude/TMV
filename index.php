<?php
/* TrouveMaVideo -- 2019
 * Version 0.2
 *
 * index.php
 * Auteur : Acryline Erin
 * acryline@protonmail.com
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 *
*/
session_start();
include_once ("includes/fonctions.php");
//creerTables();
//creerInfosAdmin();

if(!isset($_SESSION['nbr_videos_page'])) {
	$_SESSION['nbr_videos_page']=10;
}

//MENU
$page = 'Rechercher';
if (isset($_SESSION['page'])) {
	$page = $_SESSION['page'];
}
if (isset($_POST['bouton'])) {
	$page = $_POST['bouton'];
}
if (isset($_POST['aide_x']) && isset($_POST['aide_y'])) {
	$page = 'Aide';
}
if (isset($_POST['apropos_x']) && isset($_POST['apropos_y'])) {
	$page = 'Apropos';
}
if (isset($_POST['contact_x']) && isset($_POST['contact_y'])) {
	$page = 'Contact';
}
if (isset($_POST['connexion_x']) && isset($_POST['connexion_y'])) {
	$page = 'Connexion';
}
//ADMINISTRATION
//ENREGISTRER UN RÉFÉRENCEMENT DES VIDÉOS D'UNE INSTANCE DANS LA BDD
$_SESSION['message_ref'] = '';
if (isset($_POST['refInstance']) && isset($_POST['urlvideo'])) 
{
   $url = $_POST['urlvideo'];
   $reponse = testInstancePeertube($url);	
   if($reponse)
   {
   	$_SESSION['message_ref'] = saveInstancePeertube($reponse, $url);
   }else{
		$_SESSION['message_ref'] = "Erreur d'URL";   
   }
	$page = 'Administration';
}
//ADMINISTRATION
//MISE A JOUR MANUELLE DES INSTANCES
if(isset($_POST['refInst']) && isset($_POST['maj']) && isset($_POST['video1']) && isset($_POST['video2']))
{
	$idInst = $_POST['refInst'];
	$nbrVideos = $_POST['video1'];
	$total = $_POST['video2'];
   majInstancePeertube($idInst,$nbrVideos,$total);
	$page = 'Administration';
}
//ADMINISTRATION
//MISE À JOUR DES VIDÉOS
$_SESSION['message_maj_video']= 0;
if(isset($_POST['MajVideos']))
{
  $_SESSION['message_maj_video'] = majVideosPeertube(); //A FAIRE
  $page = 'Administration';
}
//ADMINISTRATION
//NETTOYAGE DES VIDEOS
$_SESSION['message_nettoyage_video']= 0;
if(isset($_POST['fantomes']))
{
  	$_SESSION['message_nettoyage_video']= videosFantomesPeertube();//A TESTER
  	$page = 'Administration';
}
//ADIMINISTRATION
//SUPPRIMER UNE INSTANCE
$_SESSION['message_suppr_inst']='';
if(isset($_POST['suppInstance']) && isset($_POST['valSupp']))
{
  $idInst = $_POST['suppInstance'];
  if($idInst > 0)
  {
	  $_SESSION['message_suppr_inst']=supprInstancePeertube($idInst);
  }
  $page = 'Administration';
}
//RECHERCHE
unset($_SESSION['Instance']);
unset($_SESSION['mot-cle']);
unset($_SESSION['catego']);
unset($_SESSION['parution']);
unset($_SESSION['chaine']);
unset($_SESSION['debut']);
unset($_SESSION['fin']);
unset($_SESSION['tpsInf']);
unset($_SESSION['tpsSup']);
unset($_SESSION['langue']);
unset($_SESSION['licence']);
unset($_SESSION['tri']);
unset($_SESSION['SFW']);
if (isset($_POST['recherche'])) {
	if (isset($_POST['Instance'])) {
		$_SESSION['Instance'] = $_POST['Instance'];
	}
	if (isset($_POST['mot-cle'])) {
		$_SESSION['mot-cle'] = $_POST['mot-cle'];
	}
	if (isset($_POST['catego'])) {
		$_SESSION['catego'] = $_POST['catego'];
	}
	if (isset($_POST['parution'])) {
		$_SESSION['parution'] = $_POST['parution'];
	}
	if (isset($_POST['chaine'])) {
		$_SESSION['chaine'] = $_POST['chaine'];
	}
	if (isset($_POST['debut'])) {
		$_SESSION['debut'] = $_POST['debut'];
	}
	if (isset($_POST['fin'])) {
		$_SESSION['fin'] = $_POST['fin'];
	}
	if (isset($_POST['tpsInf'])) {
		$_SESSION['tpsInf'] = $_POST['tpsInf'];
	}
	if (isset($_POST['tpsSup'])) {
		$_SESSION['tpsSup'] = $_POST['tpsSup'];
	}
	if (isset($_POST['langue'])) {
		$_SESSION['langue'] = $_POST['langue'];
	}
   if (isset($_POST['licence'])) {
		$_SESSION['licence'] = $_POST['licence'];
	}
	if (isset($_POST['tri'])) {
		$_SESSION['tri'] = $_POST['tri'];
	}
	if (isset($_POST['SFW'])) {
		$_SESSION['SFW'] = $_POST['SFW'];
	}
	$page = 'Rechercher';
}
//CONNEXION
if (!isset($_SESSION['TMV'])) {
	$_SESSION['TMV'] = FALSE;
}
if (isset($_POST['connexion'])) {
	if (isset($_POST['nom']) && isset($_POST['mdp'])) {
		$nom = $_POST['nom'];
		$mdp = $_POST['mdp'];
		if (verifConnexion($nom, $mdp)) {
			$_SESSION['TMV'] = TRUE;
		} else {
			$_SESSION['TMV'] = FALSE;
		}
	} else {
		$_SESSION['TMV'] = FALSE;
	}
	$page = 'Connexion';
}
//DÉCONNEXION
if (isset($_POST['deconnexion'])) {
	$_SESSION['TMV'] = FALSE;
	$page = 'Connexion';
}

//CONTACT
$_SESSION['contact'] = '';
if (isset($_POST['envoie_contact']) && isset($_POST['urlInst'])
						&& isset($_POST['email']) && isset($_POST['captcha'])) 
{
	$url = $_POST['urlInst'];
	$email = $_POST['email'];
	$captcha = $_POST['captcha'];
	$_SESSION['contact'] = gestionContact($url,$email,$captcha);
	$page = 'Contact';
}

//DESSIN DE LA PAGE
include ("includes/header.inc.php");
echo " <div class='bas'>";
switch ($page) {
	case 'Administration':
		include ("includes/referencer.inc.php");
	break;
	case 'Rechercher':
		include ("includes/rechercher.inc.php");
	break;
	case 'Aide':
		include ("includes/aide.inc.php");
	break;
	case 'Apropos':
		include ("includes/apropos.inc.php");
	break;
	case 'Contact':
		include ("includes/contact.inc.php");
	break;
	case 'Connexion':
		include ("includes/connexion.inc.php");
	break;
	default:
		include ("includes/rechercher.inc.php");
}
include ("includes/footer.inc.php");
