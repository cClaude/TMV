#!/usr/bin/php
<?php
  include ("params.php");
  include ("fonctions.php");
  //Charger toutes les instances
  $inst = chargerInstances();
  $nbrInst = count($inst);
  $date = date('d-m-Y:H:i:s');
  debogue('maj_auto','LANCEMENT', $date);
  for($i=0;$i<$nbrInst;$i++)
  {
    $urlAPI = 'https://' . $inst[$i]['URLInst'] . '/api/v1/videos?start=0&count=0&filter=local';
    if (urlExiste($urlAPI))
    { 
		 $json = file_get_contents($urlAPI);
		 $tab = json_decode($json,true);
		 $total = $tab['total'];
		 $nbrVideos = $inst[$i]['NbrVideo'];
		 debogue('maj_auto','VIDEOS', $total-$nbrVideos);
		 if($total > $nbrVideos) 
		 {
			 $idInst = $inst[$i]['IDInst'];
			 majInstancePeertube($idInst,$nbrVideos,$total);
			 debogue('maj_auto','MAJ', 'Mise à jour de '. $inst[$i]['NomInst']);
		 }
	 }		 
  }
?>