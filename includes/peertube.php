<?php
/*================================================================*/
/* Charger les informations d'une vidéo Peertube                  */
/*================================================================*/
function saveInstancePeertube( $json, $url) 
{
	/*Lancer un script php en arrière plan via une table de tâches. 
		1. Enregistrer la tâche dans la table (id, commande, encours)
		2. Tester si une tâche est en cours 
			--> si oui attendre
			--> si non : lancer la tâche
		3.Le script termine par un test : tâche existe ?
			--> oui : lancer la nouvelle tâche
			--> non : Fin
	*/

	include ("params.php");
	//Enregistrer la tâche dans la table Taches
	$tab = json_decode($json,true);
	$total = $tab['total'];
	$commande = 'php -f '.$SITE.'includes/refVideos.php '.$total.' '.$url.' &';
	$sql = "INSERT INTO `Taches` (`Commande`,`Encours`) VALUES('$commande','0');";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);	

	//Tester si une commande est en cours
	$sql = "SELECT * FROM `Taches` WHERE `Encours`='1';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);

	if(!$result)
	{
		//Trouver la première commande 
		$sql = "SELECT * FROM `Taches`";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
		//Lancer la commande 
		$commande = $result[0]['Commande'];

		system( $commande );
	}	
}	
/*================================================================*/
/* Tester si une URL correspond à la vidéo d'une instance Peertube*/
/*================================================================*/
function testInstancePeertube($url) {
	
	//API : https://docs.joinpeertube.org/api.html
	$result = parse_url($url);

	if (!isset($result['host'])) {
		debogue('peertube_erreur', 'URL', 'Aucun hôte trouvé.');
		return FALSE;
	}
	$urlAPI = $result['scheme'] . '://' . $result['host'] . '/api/v1/videos?start=0&count=0&filter=local';

	if (!urlExiste($urlAPI)) {
		debogue('peertube_erreur', 'URL', 'Cette URL n\'existe pas :'.$urlAPI);
		return FALSE;
	}
	$json = file_get_contents($urlAPI);
	return $json;
}
/*================================================================*/
/* Charger les informations des vidéos référencées                */
/*================================================================*/
function chargerInfosVideos($idInst, $idChaine,$idCat,$motCle,$parution,  $debut, $fin, $tpsInf, $tpsSup, 
																		       	$idLicence,$idLangue, $tri,$sure)
{
   include ("params.php");
   if($sure){$sfw = 1;}else{ $sfw = 0;}
   $dateDef= [NULL,NULL];	
   $sql="";
	
	if ($tpsInf > 0 || $tpsSup > 0) {
		if ($tpsSup <= $tpsInf) {
			$tpsSup = $tpsInf + 1;
		}
		$dureeTab = [intval($tpsInf) * 60, intval($tpsSup) * 60];
	}
	
	//Tous publics
	$sql = "SELECT * FROM `InfosVideos` WHERE `SFW`='$sfw' ";
	//Instance
	if ($idInst > 0 ) {
		$sql .= " AND `IDInst` = '$idInst' ";
	}
	//Chaîne
	if ($idChaine) 
	{
		$sql .= " AND `IDChaine` ='$idChaine' ";
   }
	//Catégorie
	if($idCat > 0) {
		$sql .= " AND `IDCat`= '$idCat' ";
	}	
	//Période
	if ($parution || $debut || $fin) 
	{
		$dateDef = defDate($parution, $debut, $fin);
	}
	if ($dateDef[0]) 
	{
		if (strtotime($dateDef[0]) == strtotime($dateDef[1])) 
		{
			$jour = substr($dateDef[0], 0, 10);
			$sql .= " AND `Parution` LIKE '$jour%'";
		} else {
			$sql .= " AND `Parution` BETWEEN '$dateDef[1]' AND '$dateDef[0]' ";
		}
}
	//Licence
	if($idLicence > 0) {
		$sql .= " AND `IDLicence`= '$idLicence' ";
	}	
	//Langue
	if($idLangue > 0) {
		$sql .= " AND `IDLangue`= '$idLangue' ";
	}
	//Mot-cle
	if($motCle)
   {
   	$motCle = htmlentities($motCle);
   	$sql .= " AND UPPER(CONCAT(`Titre`,`Description`)) LIKE UPPER('%$motCle%') ";
   }
 	//Filtrer les instances non connectées
  	$instOn = instancesOn();  
  	$chaine = implode(', ', $instOn);	
	$sql .= "AND `IDInst` IN ( $chaine ) ";	
	//Tri
	if($tri == 1 )$sql .=' ORDER BY `Parution` DESC ;';
	if($tri == 2 )$sql .=' ORDER BY `creation` DESC ;';
	if($tri == 3 )$sql .=' ORDER BY `Actualisation` DESC ;';
	if($tri == 4 )$sql .=' ORDER BY `Vues` DESC ;';
	if($tri == 5 )$sql .=' ORDER BY `Likes` DESC ;';
	if($tri == 6 )$sql .=' ORDER BY `Dislikes` DESC ;';
	if($tri == 7 )$sql .=' ORDER BY `Titre` ASC ;';
	if($tri == 8 )$sql .=' ORDER BY `Duree` DESC ;';
	 
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD); 

	return $result;
}
/*================================================================*/
/* Mettre à jour une instance                                     */
/*================================================================*/
function majInstancePeertube($idInst,$nbrVideos,$total)
{
   if($nbrVideos == $total) {return false;}
   include ("params.php");   
   //Charger l'url de l'instance
   $result = trouverInst($idInst);
   $host = $result['URLInst'];
   $count = $total-$nbrVideos;
   $urlAPI = "https://".$host."/api/v1/videos?start=0&count=".$count."&filter=local&sort=-publishedAt";
   $json = file_get_contents($urlAPI);
   $tableau = json_decode($json,true);
   $rep = print_r($tableau,true);
   for($i=0;$i<$count;$i++)
   {
   	saveVideo($idInst, $tableau,$i,$host,$total);
   }
   return true;
}
/*================================================================*/
/* Mettre à jour les vidéos enregistrées si nécessaire            */
/*================================================================*/
function majVideosPeertube()
{
  $nbrMaj = 0; 
  include ("params.php");

  return $nbrMaj;
}
/*================================================================*/
/* Nettoyer les vidéos fantômes                                   */
/*================================================================*/
function videosFantomesPeertube()
{
  $nbrFantomes = 0;
  include ("params.php");
  
  //Informations sur les instances
  $result = chargerInstances();
  $nbrInst = count($result);
  
  //Traiter chaque instance
  for($i=0;$i<$nbrInst;$i++)
  {
    $nbrVideos = $result[$i]['NbrVideo'];
    $host = $result[$i]['URLInst'];
    //Tester si l'instance est disponible.
    if (!urlExiste($host)) break;
    //Nombre effectif de vidéo de l'instance
    $urlAPI = "https://".$host."/api/v1/videos?start=0&count=0&filter=local";
    $json = file_get_contents($urlAPI);
    $tableau = json_decode($json,true);
    $total = $tableau['total'];
    //Supprimer les vidéos s'il existe une différence
    if($total < $nbrVideos)
    {
      $idInst = $result[$i]['IDInst'];
      //Trouver toutes les URL de l'instance
      $sql = "SELECT * FROM `URLVideos` WHERE `IDInst`='$idInst';";
      $videos = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
      $nbr =count($videos);
      for($j = 0;$j<$nbr;$j++)
      {
        $url = $videos[$j]['URL'];
        $idURL =  $videos[$j]['IDURL'];
        $headers = get_headers($url);
        $code = explode(' ',$headers[0]);
        if($code[1] !=200 AND $code[1]!=301)
        {
			 //Supprimer l'URL dans URLVideos
			 $sql = "DELETE FROM `URLVideos` WHERE `IDURL`='$idURL';";
			 enregistrer($sql, $HOST, $USER, $MDP, $BDD);
			 //Supprimer la vidéos dans InfosVideos 
			 $sql = "DELETE FROM `InfosVideos` WHERE `IDurl`='$idURL';";
			 enregistrer($sql, $HOST, $USER, $MDP, $BDD); 
			 $nbrFantomes++;      
        }    
      }  
    
    }
  }
  
  return $nbrFantomes;
}
/*================================================================*/
/* Supprimer une instance Peertube                                */
/*================================================================*/
function supprInstancePeertube($idInst)
{
	include ("params.php");
	$sql = "DELETE  FROM  `URLVideos` WHERE `IDInst`='$idInst';";
	$result = enregistrer($sql, $HOST, $USER, $MDP, $BDD);
   
	$sql = "DELETE  FROM `InfosVideos` WHERE `IDInst`='$idInst';";
	$result = enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	
	$sql = "DELETE  FROM  `Chaines` WHERE `IDInst`='$idInst';";
	$result = enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	
	$sql = "DELETE  FROM  `Instances` WHERE `IDInst`='$idInst';";
	$result = enregistrer($sql, $HOST, $USER, $MDP, $BDD);

	$message = "L'instance a été supprimée.";
	return $message;
}
?>