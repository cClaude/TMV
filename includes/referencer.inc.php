<?php
	$tabInst = chargerInstances();
	$nbrInst = count($tabInst);
?>
<section id ='reference'>
<div class='w3-row'>
 <div class=' w3-third  '>
	     <div class='referencer w3-white w3-display-container bordBleu' >	
			<div class='zone_admin ' >
			   <h3>Référencer une instance</h3>
				<label>URL de l'instance Peertube, sans espace au début.</label>
				<form action='index.php' method='POST'>
					<input  type='url'  name='urlvideo' style='width:100%;'><br><br>
					<input  type ='submit' name='refInstance' value = 'Valider' 
					  class='w3-button w3-hover-light-blue w3-text-white btnbleu' > <br><br><br><br>
					  <div class='w3-display-bottommiddle'>
					  <label class='w3-text-black'><?php echo $_SESSION['message_ref']; ?><br><br></label>
				      </div>
			   </form>
			</div>
		</div>
	</div>
	<div class='referencer w3-third w3-white w3-display-container bordBleu'>
	    <div class='zone_admin' id='idMaj'>
			  <h3>Mettre à jour les vidéos manuellement</h3>
	   	    <form action='index.php' method='POST'>
					<label>Instance Peertube</label><br>
					 <select class='w3-select' name='refInst' style='width:98%' id='idRefInst' onchange=' majVideos();' >
					 	<option value='0' selected >Aucune</option>
					 	<?php 
					 		for ($i = 0;$i < $nbrInst;$i++) 
					 		{
							 	echo "<option value='".$tabInst[$i]['IDInst']."'>".$tabInst[$i]['NomInst']."</option>"; 
							}
					 	?>
					 </select><br><br>			     	   	    
	   			<label>Vidéos enregistrées </label>
	   	 		<input type='number' name='video1' value='0'><br>
	   	 		<label>Vidéos de l'instance </label>
	   	 		<input type='number' name ='video2' value ='0'><br><br>
	   	 		<input type='submit' name='maj' DISABLED  value = 'Mettre à jour' 
	   	 		class='w3-button w3-hover-light-blue w3-text-white btnbleu'>
	   	    </form>
	   	</div>
	</div>
	<div class=' referencer w3-third  w3-white w3-display-container bordBleu'>
		<div class='zone_admin ' >
			 <h3>Supprimer une instance Peertube</h3>
			 <form action='index.php' method='POST'>
			 <label>Instance Peertube</label><br>
				 <select class='w3-select' name='suppInstance' style='width:98%' >
				 	<option value='0' selected >Aucune</option>
				 	<?php 
				 		for ($i = 0;$i < $nbrInst;$i++) 
				 		{
						 	echo "<option value='".$tabInst[$i]['IDInst']."'>".$tabInst[$i]['NomInst']."</option>"; 
						}
				 	?>
				 </select><br><br>	
				  <input  type ='submit' name='valSupp' value = 'Supprimer' 
				  class='w3-button w3-hover-light-blue w3-text-white btnbleu' > <br><br><br>
				  <div class='w3-display-bottommiddle'>
				  <label class='w3-text-black'><?php echo $_SESSION['message_suppr_inst']; ?><br><br></label>
				  </div>
			 </form>
		</div>	
	</div>	
</div>
<div class ='w3-row'>
	<div class='referencer w3-third w3-white w3-display-container bordBleu'>
		<div class='zone_admin ' >
			 <h3>Mettre à jour les vidéos enregistrées</h3>
			 <form action='index.php' method='POST'>
			 	<input  type ='submit' name='MajVideos' value = 'Lancer la mise à jour' 
					  class='w3-display-middle w3-button w3-hover-light-blue w3-text-white btnbleu' > 
			 </form>
			 <div class='w3-text-black'>
			 	<?php echo $_SESSION['message_maj_video']." vidéo(s) mises à jour."; ?>
			 </div>  
		</div>
	</div>
	<div class='referencer w3-third w3-white w3-display-container bordBleu'>
		<div class='zone_admin ' >
			 <h3>Supprimer une vidéo</h3>
		</div>
	</div>
	<div class='referencer w3-third w3-white w3-display-container bordBleu'>
		<div class='zone_admin ' >
			 <h3>Nettoyer les vidéos fantômes</h3>
			 <form action='index.php' method='POST'>
			 	 <input  type ='submit' name='fantomes' value = 'Lancer le nettoyage' 
					  class='w3-display-middle w3-button w3-hover-light-blue w3-text-white btnbleu' > 
			 </form>
   		  <div class='w3-text-black'>
			 	<?php echo $_SESSION['message_nettoyage_video']." vidéo(s) nettoyées."; ?>
			 </div>
		</div>
	</div>
</div>
</section>