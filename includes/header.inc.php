<!-- En-tête -->

<!DOCTYPE html>

<html >
	<head>
		<title>TrouveMaVideo</title>
		<meta charset=utf-8>
		<link rel="stylesheet" href="css/w3.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="shortcut icon" href="favicon.ico" >
   	<link rel="icon" type="image/gif" href="animated_favicon1.gif" >
	</head>
	<body > 
	<header class='haut'>
		 <div class='w3-row' >
		 	<div class='w3-half'>
				<div class="w3-left" style='padding:3px;'>
					<a href='index.php'><img src="images/logoTMV.png" alt="TrouveMaVideo" width="200px" ></a>
				</div>
			</div>
			<div class='w3-half'>
		 		<div class="w3-padding w3-right ">
		 			<div class='w3-card-2'>
		 				<ul class='w3-ul w3-white w3-small'>
		 					<li>Peertube :  <?php echo nbrVideos(); ?> vidéos</li>
		 				</ul>
		 			</div>
		 		</div>
			</div>
		 </div>
		 <div class='menu'>
		 <form action="index.php" method="POST" >
		 	 <div class="w3-row w3-card-2 barMenu">
		 	 		<div class="w3-threequarter">
		 	 			<div class="w3-bar  w3-xlarge  w3-display-container btn-action ">
		 	 					<input type="submit" class="w3-button w3-hover-light-blue w3-text-white " 
		 	 					name = "bouton" value="Vidéos">
		 	 					<?php if ($_SESSION['TMV']) { ?>
		 	 					<input type="submit" class="w3-button w3-hover-light-blue w3-text-white " 
		 	 					name = "bouton" value="Administration">
		 	 					<?php
								} ?>
		 	 			</div>
		 	 		</div>
		 	 		<div class="w3-quarter">
							   <input type="image" class="w3-button w3-hover-light-blue w3-text-white " 
							   src="images/aide.png" name = "aide" alt="aide">
								<input type="image" class="w3-button w3-hover-light-blue w3-text-white " 
							   src="images/apropos.png" alt="infos" name = "apropos">
								<input type="image" class="w3-button w3-hover-light-blue w3-text-white " 
							   src="images/mail.png" alt="Contact" name = "contact">
								<input type="image" class="w3-button w3-hover-light-blue w3-text-white " 
							   src="images/log-in.png" alt="Connexion" name = "connexion">
		 	 		</div>
		 	 </div>
		 </form>
		 </div>
	</header>
<script src="js/script.js" ></script>