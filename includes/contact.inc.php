<!--Contacter l'administrateur -->

<section>
<div class='referencer w3-white w3-display-container w3-padding' >
	<div class=" w3-medium mapage" >
		<div class='referencer w3-white w3-display-container' >
		  <div class='bordBleu w3-padding'><h2>Proposer une nouvelle instance Peertube</h2></div>
			<p>Cette page vous permet de proposer une instance <b>Peertube</b> qui n'est pas encore référencée sur <b>TrouveMaVidéo</b>.</p>
			<p>Pour ce faire, entrez l'adresse de la nouvelle instance Peertube ci-dessous.<br>
			 Vous pouvez également indiquer une adresse email qui sera utilisée uniquement en 
			 cas de problème ou pour obtenir des précisions sur l'instance proposée.</p>
	        <div class='formContact'>			 
				<form  class="w3-container w3-card-4 w3-light-grey w3-margin" 
							action='index.php' method='POST' name='contact'>
					<h4 class="w3-center">Nouvelle instance Peertube</h4>
					<br><input class='w3-input' type='url' name='urlInst' placeholder="URL de l'instance proposée">
					<br><input class='w3-input' type='email' name='email'  placeholder='Adresse email (facultative)'>
					<br><br>
					 <?php captcha();?> Retranscrire le nombre :
		            <input class=" w3-white" type="text" name="captcha" size="5"><br><br>
					<div class='w3-text-black' > <?php echo $_SESSION['contact']; ?> </div>
					<input class='w3-btn  w3-text-white btnbleu' type='submit' name='envoie_contact' value='Envoyer'>
					<br><br>					
				</form>	
			  </div>
		</div>
	</div>
</div>
</section>