<!-- Connecter l'administrateur  https://www.php.net/manual/fr/function.crypt.php -->
<section>
	<div class='referencer w3-white w3-display-container' >
		<div class = 'w3-card-2 w3-display-middle w3-padding' style='width:60%;'>
			<h4 class='btnbleu w3-text-white w3-padding'>Connexion de l'administrateur</h4>
			<form action='index.php' method='POST'>
				<?php if (!$_SESSION['TMV']) { ?>
					<label> Nom </label>
					<input type='text' name ='nom' class='w3-input' style='width:95%;'>
				<label> Mot de passe </label>
				<input type='password' name ='mdp' class='w3-input' style='width:95%;'>
				<br>
				<input class= 'w3-button w3-hover-light-blue w3-text-white btnbleu'
			                     type='submit' name ='connexion'  value='Connexion' 
			                     style='width:150px;'>
				<?php
				} else {
					echo "Vous êtes connecté.<br>";
					echo "<br><br><input class= 'w3-button w3-hover-light-blue w3-text-white btnbleu w3-display-bottomright w3-margin'
				                     type='submit' name ='deconnexion'  value='Déconnexion' 
				                     style='width:150px;'>";
				}
				?> 
			</form>
		</div>
	</div>
</section>