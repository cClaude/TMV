<!-- Page de recherche par critères -->
<?php
$idCat = 0;
$motCle = NULL;
$parution = 0;
$idChaine = 0;
$idInst = 0;
$debut = NULL;
$fin = NULL;
$tpsInf = NULL;
$tpsSup = NULL;
$idLangue = 0;
$idLicence = 0;
$sure = NULL;
$tri = 1;

if (isset($_SESSION['Instance'])) {
	$idInst = $_SESSION['Instance'];
}
if (isset($_SESSION['mot-cle'])) {
	$motCle = $_SESSION['mot-cle'];
}
if (isset($_SESSION['catego'])) {
	$idCat = $_SESSION['catego'];
}
if (isset($_SESSION['parution'])) {
	$parution = $_SESSION['parution'];
}
if (isset($_SESSION['chaine'])) {
	$idChaine = $_SESSION['chaine'];
}
if (isset($_SESSION['debut']) && isset($_SESSION['fin'])) {
	$debut = $_SESSION['debut'];
	$fin = $_SESSION['fin'];
	echo $debut;
	echo $fin;
}
if (isset($_SESSION['tpsInf']) && isset($_SESSION['tpsSup'])) {
	$tpsInf = $_SESSION['tpsInf'];
	$tpsSup = $_SESSION['tpsSup'];
}
if (isset($_SESSION['langue'])) {
	$idLangue = $_SESSION['langue'];
}
if (isset($_SESSION['licence'])) {
	$idLicence = $_SESSION['licence'];
}
if (isset($_SESSION['tri'])) {
	$tri = $_SESSION['tri'];
}
if (isset($_SESSION['SFW'])) {
	$sure = $_SESSION['SFW'];
}
?>
<section>
  <div class='w3-row'>
  		 <div class="w3-quarter w3-padding w3-card-2" style="background-color:#eeeeee">
  		      <div class="w3-hide-medium w3-hide-large"> <br><br><br><br></div>
  		      <div class="w3-hide-small w3-hide-large"> <br><br></div>
				<?php formulaireRecherche(); ?>
			  <br><div>
					<script src="https://sense.framasoft.org/sense3.js" 
					data-sense3="300x250;adblock,art,browser,chaplin,cinema,
					degooglisons,elementary,encyclopedia,firefox,framabookin,
					france,generic,green,india,linux,movie,mozilla,netherlands,
					painting,social,system,ubuntu,vangogh,wikipedia;
					#ffffff,#7eb6e5,#333333,#757575,#2175bb,#cccccc">
			  	    </script>
			  </div>
  		 </div>
  		 <div class="w3-threequarter">
				<div id='tableau'>
						<?php afficherRecherche(1, 1, $idCat ,$motCle, $parution, $idChaine, $idInst, 
		 								$debut, $fin, $tpsInf, $tpsSup, $idLangue,
										$idLicence, $tri, $sure); ?>
				</div>
  		 </div>
  </div>
</section>