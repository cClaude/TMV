<?php
/**
 Fonctions php du site
 PHP version 7.3
 @category Videos
 @package  Package
 @author   acryline erin <>
 @license  CC BY-NC 3.0 FR https://creativecommons.org/licenses/by-nc/3.0/fr/
 @link     https://cogito.no-ip.info/cogito/trouvemavideo/
 */
require "bdd.php";
require "peertube.php";

//**************************************************************************
//                                 OUTILS
//**************************************************************************
//============================================================
// Tester si une URL existe
//============================================================
/*function urlOn($url) {
	$headers = @get_headers($url, 1);
	if ($headers[0] == '') return false;
	return !((preg_match('/404/', $headers[0])) == 1);
}*/

function urlExiste ($url){
  $timeout = 1;
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  $resultat = curl_exec($ch);
  $CurlErr = curl_error($ch);
  curl_close($ch);
  if ($CurlErr) {
    debogue('fonctions_erreur','URL_EXISTE ',$CurlErr);
    return false;
  }else{
    return true;
  }
}
//============================================================
//Lire un fichier txt entier
//=============================================================
function lireDoc($adresse) {
	$texte = "";
	if (!file_exists($adresse)) {
		return "Pas de texte";
	}
	$fp = fopen($adresse, "r");
	while (!feof($fp)) {
		$texte.= fgets($fp, 4096);
	}
	fclose($fp);
	return $texte;
}
//============================================================
//Ecrire dans un fichier txt
//=============================================================  
function ecrire($adresse,$texte,$mode)
{
 $fichier = fopen($adresse, $mode);
 fwrite ( $fichier , $texte );
 fclose($fichier);
}
//============================================================
//Logs du site
//=============================================================
function debogue($destination, $type, $message) 
{
   require "params.php";   
	$temps = date('d-m-Y:H-i-s');
	$adresse = $SITE.'log/'. $destination .'.txt';
	$fichier = fopen($adresse, 'a');
	fwrite($fichier, $temps . ':' . $type . '--' . $message . "\r\n");
	fclose($fichier);
}
//============================================================
//Trouver l'interval de date à rechercher
//=============================================================
function defDate($parution, $debut, $fin) {
	$dateDef = NULL;
	//validité de debut et fin
	if (isset($debut) && isset($fin)) {
		if ($debut != '' && $fin != '') {
			if (preg_match('/[0-9]{4}\-[01][0-9]\-[0-3][0-9]/', $debut) !== false && preg_match('/[0-9]{4}\-[01][0-9]\-[0-3][0-9]/', $fin) !== false) {
				$tdeb = strtotime($debut);
				$tfin = strtotime($fin);
				if ($tdeb <= $tfin) {
					$dateDef[1] = date('Y-m-d 00:00:00', $tdeb);
					$dateDef[0] = date('Y-m-d 00:00:00', $tfin);
					return $dateDef;
				}
			}
		}
	}
	//Si debut et fin pas valides
	switch ($parution) {
		case 1:
			$dateDef[0] = $dateDef[1] = date('Y-m-d 00:00:00');
		break;
		case 2:
			$dateDef[0] = $dateDef[1] = date('Y-m-d 00:00:00', (time() - 86400));
		break;
		case 3:
			$dateDef[0] = $dateDef[1] = date('Y-m-d 00:00:00', (time() - 172800));
		break;
		case 4:
			$dateDef[0] = $dateDef[1] = date('Y-m-d 00:00:00', (time() - 259200));
		break;
		case 5:
			$dateDef[0] = date('Y-m-d 00:00:00');
			$dateDef[1] = date('Y-m-d 00:00:00', (time() - 604800));
		break;
		case 6:
			$dateDef[0] = date('Y-m-d 00:00:00');
			$dateDef[1] = date('Y-m-d 00:00:00', (time() - 2592000));
		break;
		case 7:
			$dateDef[0] = date('Y-m-d 00:00:00');
			$dateDef[1] = date('Y-m-d 00:00:00', (time() - 31536000));
		break;
		default:
			$dateDef[0] = NULL;
			$dateDef[1] = NULL;
	}
	return $dateDef;
}
//============================================================
// Afficher la pagination de la recherche
//=============================================================
function pagination($page, $nbrPages) {
	$avant = 0;
	$apres = - 1;
	$classe = [];
	$pDebut = $page - 2;
	if ($pDebut < 1) {
		$pDebut = 1;
	}
	$pFin = $pDebut + 5;
	if ($pFin > $nbrPages) {
		$pFin = $nbrPages;
	}
	echo "<br><div class='center'>";
	echo "<div class='w3-bar w3-tiny'>";
	echo "<a href='#' class='w3-button' onclick='paginer(" . $page . "," . $avant . ");'>&laquo;</a>";
	for ($i = $pDebut;$i < $pFin + 1;$i++) {
		if ($page == $i) {
			$classe[$i] = 'w3-button btnbleu w3-text-white';
		} else {
			$classe[$i] = 'w3-button';
		}
		echo "<a href='#' class='w3-button " . $classe[$i] . "' onclick='paginer(" . $page . "," . $i . ");'>" . $i . "</a>";
	}
	echo "<a href='#' class='w3-button' onclick='paginer(" . $page . "," . $apres . ");'>&raquo;</a>";
	echo "</div>";
	echo "</div>";
}
//============================================================
//Convertir un timestamp en Heures Minutes Secondes
//=============================================================
function duree2HMinSec($tps) {
	$reste = 0;
	$tabTps = ['heure' => 0, 'min' => 0, 'sec' => 0];
	if ($tps) {
		$tabTps['heure'] = floor($tps / 3600);
		$reste = (($tps / 3600) - $tabTps['heure']) * 60;
		$tabTps['min'] = floor($reste);
		$reste = ($reste - $tabTps['min']) * 60;
		$tabTps['sec'] = ceil($reste);
	}
	return $tabTps;
}

 //============================================================
 //Capcha : vérifier si l'utilisateur n'est pas un robot
 //=============================================================   
  function captcha()
  {
   $capcha = rand(10000,99999);
   $font ="fonts/LobsterTwo-Regular.ttf";
   $path ="captcha/captcha.jpg";
   
   $im = imagecreatetruecolor(100, 40);
   $bleu = imagecolorallocate($im,126, 182, 229);
   $bleuroi =  imagecolorallocate($im,255, 255, 255);
  
   imagefilledrectangle($im, 0, 0, 99, 39, $bleu);
   imagefttext($im, 20, 0, 10, 30, $bleuroi, $font,$capcha);
   
   imagejpeg($im,$path);
   imagedestroy($im);
   
   ecrire("captcha/captcha.txt",$capcha, "w+");

   echo "<img src='captcha/captcha.jpg' alt='captcha'>";
   
  }
  
//**************************************************************************
//												RECHERCHE
//**************************************************************************
//============================================================
//Afficher le formulaire de recherche
//=============================================================
function formulaireRecherche() 
{
	require "params.php"; 
	$tabCat = chargerCategos();
	$nbrCat = count($tabCat);
	$tabInst = chargerInstances();
	$nbrInst = count($tabInst);
	$tabChaine = chargerChaines();
	$nbrChaine = count($tabChaine);
	$tabLangue = chargerLangues();
	$nbrLangue = count($tabLangue);
	$tabLicence = chargerLicences();
	$nbrLicence = count($tabLicence);
	?>
	<form action ='index.php' method='POST' class='w3-small' >
	   <!-- Bouton -->
	 	<br><input class='w3-button w3-hover-light-blue w3-text-white btnbleu'
		           type='submit' name='recherche' value ='Lancer la recherche' ><br>
		<!-- Mot-clé -->
		<br><label>1 mot-clé</label><br>
		<input class='w3-input' type='text' size='26' name='mot-cle' >		
		<!-- Instance -->
			<br><label>Instance Peertube</label><br>
			 <select class='w3-select' name='Instance' id='idInstance' 
			 		style='width:98%' onchange='adapteInstance();'>
			 	<option value='0' selected >Toutes</option> 
			 	<?php 
			 		for ($i = 0;$i < $nbrInst;$i++) 
			 		{
					 	echo "<option value='" . $tabInst[$i]['IDInst'] . "'>" . $tabInst[$i]['NomInst'] . "</option>";
					}
			 	?>
			 </select>
			<!-- Chaînes -->
			<label>Chaîne</label><br>
				<select class='w3-select' name='chaine' id='idChaine' style='width:98%' >
					<option value='0' selected >Toutes</option>
					<?php
						for ($i = 0;$i < $nbrChaine;$i++) {
						echo "<option value='" . $tabChaine[$i]['IDChaine'] . "'>" . $tabChaine[$i]['NomChaine'] . "</option>";
						}
					?>
				</select>
		 <!-- Catégorie -->
		 <label>Catégorie</label><br>
			<select class='w3-select' name='catego' style='width:98%'>
				<option value='0' selected >Toutes</option>
				<?php	for ($i = 0;$i < $nbrCat;$i++) {
				echo "<option value='" . $tabCat[$i]['IDCat'] . "'>" . $tabCat[$i]['NomCatFr'] . "</option>";
				}?>
			</select>
  			 <label>Parution</label><br>
			<select class='w3-select' name='parution' style='width:98%'>
				<option value='0' selected >Toutes les dates</option>
				<option value='1'>Aujourd'hui</option>
				<option value='2'>Hier</option>
				<option value='3'>Il y a 2 jours</option>
				<option value='4'>Il y a 3 jours</option>
				<option value='5'>Cette semaine</option>
				<option value='6'>Ce mois</option>
				<option value='7'>Cette année</option>
			</select>
		 <label>Licence</label><br>
			<select class='w3-select' name='licence' style='width:98%'>
				<option value='0' selected >Toutes</option>
					<?php	for ($i = 0;$i < $nbrLicence;$i++) 
				{
					echo "<option value='" . $tabLicence[$i]['IDLicence'] . "'>" . $tabLicence[$i]['NomLicence'] . "</option>";
				}?>
			</select>
		 <label>Langue</label><br>
				<select class='w3-select' name='langue' style='width:98%'>
					<option value='0' selected >Toutes</option>
					<?php	for ($i = 0;$i < $nbrLangue;$i++) {
						if($LANGUE == "fr")
						{
							echo "<option value='" . $tabLangue[$i]['IDLangue'] . "'>" . $tabLangue[$i]['NomLangueFr'] . "</option>";
						}else{
							echo "<option value='" . $tabLangue[$i]['IDLangue'] . "'>" . $tabLangue[$i]['NomLangue'] . "</option>";
						}
					  }
					?>
				</select>
				<label>Tri</label><br>
				<select class='w3-select' name='tri' style='width:98%'>
					<option selected value='1'>Parution</option>
					<option value='2'>Création</option>
					<option value='3'>Actualisation</option>
					<option value='4'>Vues</option>
					<option value='5'>Likes</option>
					<option value='6'>Dislikes</option>
					<option value='7'>Titre</option>
					<option value='8'>Durée</option>
				</select><br><br>	
				<label>Période de</label><br>
				<input type='date' name ='debut' style='width:70%' >
				<label> à </label><br>
				<input type='date' name ='fin' style='width:70%' ><br><br>
			   <label>Durée en minutes de </label><br>
				<input type='number' name ='tpsInf' style='width:40%' >
				<label> à </label>
				<input type='number' name ='tpsSup' style='width:40%' >
				<br><br><input type='checkbox' name='SFW' value='Oui'  > Adulte
	</form>
	<?php
}
//============================================================
// Afficher le résultat  de la recherche
//=============================================================

function afficherRecherche($pageOld, $page, $idCat,$motCle, $parution,$idChaine, $idInst, 
$debut, $fin, $tpsInf, $tpsSup, $idLangue,$idLicence, $tri, $sure)
{
	include ("params.php");
   $infos = chargerInfosVideos($idInst, $idChaine,$idCat ,$motCle, $parution,  $debut, $fin, $tpsInf, $tpsSup, 
																		       	    $idLicence,$idLangue, $tri,$sure);
	$vidPage = $_SESSION['nbr_videos_page'];
																		       	    
	$nbrVideos = count($infos);
	$nbrPages = ceil($nbrVideos / $vidPage);
	if ($page == 0) //page avant
	{
		$page = $pageOld - 1;
		if ($page < 1) {
			$page = 1;
		}
	}
	if ($page == - 1) //page suivante
	{
		$page = $pageOld + 1;
		if ($page > $nbrPages) {
			$page = $nbrPages;
		}
	}
	$nbrDeb = ($page - 1) * $vidPage;
	$nbrFin = $page * $vidPage;
	if ($nbrFin > $nbrVideos) {
		$nbrFin = $nbrVideos;
	}
	echo "";
	
	 ?>
	 <br><table class=' w3-striped  w3-white' style='width:100%;'>
	   <div class='etiquette w3-small'>
	   		<?php 
	   				echo "<span class='w3-left w3-margin-left w3-margin-top'>".$nbrVideos." vidéo(s) trouvée(s)</span>";
	   				echo "<span class='w3-right w3-margin-right w3-margin-top'> Vidéos par page :";
	   				echo "<input class='w3-label' style='width:55px' type='number' min='1' max='200' "; 
	   				echo "id='nbrVideosPage'  size='3' value='".$vidPage."' onchange='nbrVideosPage();'></span>"; 
	   				pagination($page, $nbrPages);	
	   		?> 
	   </div>
    	<tr>
    		<th style='width:12%;'> Aperçu </th>
			<th style='width:12%;'> Titre </th>
			<th style='width:10%;'> Date </th>
			<th style='width:10%;'> Durée</th>
			<th style='width:12%;'> Chaîne</th>
			<th style='width:8%;'> Catégorie</th>
			<th style ='width:21%;' > Description </th>
	 	</tr>

	 	<?php 
	 	for ($i = $nbrDeb;$i < $nbrFin;$i++) 
	 		{
	 			$result = trouverURLVideo($infos[$i]['IDUrl']);
	 			$idInst = $result['IDInst'];
	 			$url = $result['URL'];
	 			$result = trouverInst($idInst);
	 			$instance = $result['NomInst'];
	 			$urlInst = $result['URLInst'];
	 			$image = $infos[$i]['URLPhoto'];
				$titre = strtolower($infos[$i]['Titre']);
				$titre = ucfirst(ltrim($titre));
				$parution = date("d-m-Y", strtotime($infos[$i]['Parution']));
				$tabTps = duree2HMinSec($infos[$i]['Duree']);
				$duree = $tabTps['heure'] . "h " . $tabTps['min'] . "min " . $tabTps['sec'];
				$result = trouverChaine($infos[$i]['IDChaine']);
				$chaine = $result['NomChaine'];
				$urlChaine = $result['URLChaine'];
				$result = trouverCatego($infos[$i]['IDCat']);
				if($LANGUE == 'fr') 
				{
					$catego = $result['NomCatFr'];
				}else{
					$catego = $result['NomCat'];
				}
				$description =  $infos[$i]['Description'];
				$likes =  $infos[$i]['Likes'];
				$vues =  $infos[$i]['Vues'];
				$urlMiniature = "https://".$urlInst.$image;
				$data  = file_get_contents($urlMiniature);
				$dim = getimagesizefromstring($data);
				if(!$dim)
				{
             	$urlMiniature = $DOMAINE."images/miniature-dft.png";
            }
            
	 			echo "<tr class='w3-small'>";
	 			echo "<td><a href='" . $url . "' target='_blank'><img src ='".$urlMiniature."' alt='Miniature' width='150px;'/></a></td>";
	 			echo "<td>";
	 			echo "$titre<br>";
				echo "<div class ='x3-bar'>";
				echo "<a href='" . $url . "' target='_blank'> <label class='w3-margin-left w3-margin-right w3-tiny'>" . $instance . "</label> </a>";
				echo "<br> ".$likes."<img src='images/like.png' width='16px' >";
				echo " ".$vues."<img src='images/vues.png' width='16px' >";	
				echo "</div>";	 			
	 			echo "</td>";
	 			echo "<td>" . $parution . "</td>";
	 			echo "<td>" . $duree . "</td>";
	 			echo "<td><a href='" . $urlChaine . "' target='_blank'>" . $chaine . "</a></td>";
	 			echo "<td>" . $catego . "</td>";
	 			echo "<td>".$description."</td>";
	 			echo "</tr>";
	 		}
	 	?>
	 </table>
	 <?php
	 echo "<div class='w3-white'>";
	  pagination($page, $nbrPages); 	
	 echo "<br></div>";										       	  
}