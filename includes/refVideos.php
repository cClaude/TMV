#!/usr/bin/php
<?php
  include ("params.php");
  include ("includes/fonctions.php");

  $total = $argv[1];
  if($total != 0) 
  {	
	  $url = stripslashes($argv[2]);
	  
	  //Trouver l'id de la premiere commande
	  $sql = "SELECT * FROM `Taches`";
	  $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	  $idTache =  $result[0]['IDTache'];
	  
	  //Mettre à 1 le champs Encours
	  $sql = "UPDATE `Taches` SET `Encours`='1' WHERE `IDTache`='$idTache';";
	  enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
	  
	  //Enregistrer les vidéos dans InfosVideos
		  //traiter l'URL
			$n = 0;
			$result = parse_url($url);

			if (!isset($result['host'])) {
				debogue('peertube_erreur', 'URL', 'Aucun hôte trouvé.');
				return FALSE;
			}
			//Enregistrer l'instance si elle n'existe pas
			$urlInst = $result['host'];
			$json = file_get_contents($result['scheme'] . '://' . $result['host'] . '/api/v1/config/about');
			$tableau = json_decode($json, true);
			$nomInst = $tableau['instance']['name'];
			$descInst = $tableau['instance']['description'];
		   $idInst = saveInstance($nomInst, $urlInst,$total, $descInst);
	   
		   //Traiter les vidéos de l'instance
		   for ($i = 0;$i <= $total;$i+= 10) 
		   {
		   	$urlAPI = $result['scheme'] . '://' . $result['host'] . '/api/v1/videos?start=' . $i . '&count=10&filter=local';
				$json = file_get_contents($urlAPI);
				$tableau = json_decode($json, true);
				for ($j = 0;$j < 10;$j++) 
				{
					if (isset($tableau['data'][$j])) 
					{
						//Le titre
						$titre = ucfirst(ltrim(strtolower($tableau['data'][$j]['name'])));
						//ID
						$idPT = $tableau['data'][$j]['id'];
						//UUID
						$uuid = $tableau['data'][$j]['uuid'];
						//URL --> à enregistrer dans la table
						$url =  $result['scheme'] . '://' . $result['host'] . '/videos/watch/' . $uuid;
						//Date de création
						$tps = $tableau['data'][$j]['createdAt'];
						$creation = date('Y-m-d H:i:s', strtotime($tps));
						//Date de publiction
						$tps = $tableau['data'][$j]['publishedAt'];
						$parution = date('Y-m-d H:i:s', strtotime($tps));
						//Date d'actualisation
						$tps = $tableau['data'][$j]['updatedAt'];
						$actu = date('Y-m-d H:i:s', strtotime($tps));
						//Catégories --> à enregistrer dans la table
						$categorie = $tableau['data'][$j]['category']['label'];
						//Langues --> à enregistrer dans la table
						$langue = $tableau['data'][$j]['language']['label'];
						//Licences --> à enregistrer dans la table
						$licence = $tableau['data'][$j]['licence']['label'];
						//Nom de la chaîne  --> à enregistrer dans la table
						$nomChaine = $tableau['data'][$j]['channel']['displayName'];
						//url de la chaine
						$urlChaine = $tableau['data'][$j]['channel']['url'];
						//uuid de la chaine
						$uuidChaine = $tableau['data'][$j]['channel']['uuid'];
						//Description de la chaine
						$descChaine = $tableau['data'][$j]['channel']['description'];
						//Compte de la chaine
						$compte = $tableau['data'][$j]['account']['name'];
						//Adeptes et Favoris de la chaine
						$urlCompteAPI =  $result['scheme'] . '://' . $result['host'] . '/api/v1/accounts/'.$compte;
						$jsonCompte = file_get_contents($urlCompteAPI);
						$tabCompte = json_decode ($jsonCompte,true);
						$favoris = $tabCompte['followingCount'];
						$adeptes = $tabCompte['followersCount'];
						if(!$adeptes) {$adeptes = 0;}
						if(!$favoris) {$favoris = 0;}					
						//Confiance 
						$confiance = $tableau['data'][$j]['privacy']['label'];
						//Description
						$description = $tableau['data'][$j]['description'];
						//Durée
						$duree = $tableau['data'][$j]['duration'];
						//Local
						$local = $tableau['data'][$j]['isLocal'];
						//miniature de la vidéo
						$miniature = $tableau['data'][$j]['thumbnailPath'];
						//Vues
						$vues = $tableau['data'][$j]['views'];
						//Likes
						$likes = $tableau['data'][$j]['likes'];
						//Dislikes
						$dislikes = $tableau['data'][$j]['dislikes'];
						//Safe For Work
						$sure = $tableau['data'][$j]['nsfw'];
						if(!$sure) {$sfw = 0;} else{ $sfw = 1;}
						
						$idURL = saveURLVideo($url,$idInst,1);
						$idCat = saveCatego($categorie);
						$idLangue = saveLangue($langue);
						$idLicence = saveLicence($licence);	
						$idChaine = saveChaine($idInst,$nomChaine, $urlChaine, $uuidChaine,$descChaine,$compte,$favoris,$adeptes);
						$titre = addslashes(htmlentities($titre));
						$description = addslashes(htmlentities($description));
						if($idURL !=0)
						{
                          $sql =  "INSERT INTO `InfosVideos`(`IDPT`,`IDInst`,`Titre`,`UUIDVideo`,`IDUrl`,`creation`,";
                          $sql .= "`Parution`,`Actualisation`,`IDCat`,`IDLangue`,`IDLicence`,`IDChaine`,`confiance`,";
                          $sql .= "`Description`,`Duree`,`Local`,`URLPhoto`,`Vues`,`Likes`,`Dislikes`,`SFW`,`tabCles`)";
                          $sql .= "VALUES ('$idPT','$idInst','$titre','$uuid','$idURL','$creation','$parution','$actu','$idCat',";
                          $sql .= "'$idLangue','$idLicence','$idChaine','$confiance','$description','$duree','$local',";
                          $sql .= "'$miniature','$vues','$likes','$dislikes','$sfw','');";	
                          enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
						  debogue('ref', 'VIDEOS', 'Vidéo enregistrée'.$i.'--'.$j); 				
						}					
					}
				}
			}
		
	  //Supprimer la ligne à la fin de la commande
	  $sql = "DELETE FROM `Taches` WHERE `IDTache`='$idTache';";
	  enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	  
	  //Lire la prochaine ligne  
	  $sql = "SELECT * FROM `Taches`";
	  $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	  if($result)
	  {
	    $commande = $result[0]['Commande'];
		 system( $commande );  
	  }
	}
  exit(0);

  