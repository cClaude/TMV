#!/usr/bin/php
<?php
  include ("params.php");
  include ("fonctions.php");
  
  $tDeb=microtime(true);
  
  $url ='';
  $json = '';
  $idInst = 0;
  $headers = '';
  $tab=[];  
  $code = [];
  $sql = "SELECT * FROM `URLVideos`;";
  $result =  chargerTable($sql, $HOST, $USER, $MDP, $BDD);
  $nbrVid = count($result);
  
  for($i=0;$i<$nbrVid;$i++)
  {
	 $url = $result[$i]['URL'];
	 $tab =  parse_url($url);
	 $uuid = explode('/',$tab['path']);
	 $urlAPI = $tab['scheme'] . '://' . $tab['host'] . '/api/v1/videos/'.$uuid[3];
	 //tester l'url
	 $headers = get_headers($urlAPI);
    $code = explode(' ',$headers[0]);
   if(isset($code[1]))
   {
    if($code[1] ==200 || $code[1]==301)
     {
     	 //Charger les likes et les vues
     	 $idURL = $result[$i]['IDURL'];
     	 $json = file_get_contents($urlAPI);
     	 $tab = json_decode($json,true);
     	 if($tab['likes']) {$likes = $tab['likes'];}else{$likes = 0;}
     	 if($tab['dislikes']){$dislikes = $tab['dislikes'];}else{$dislikes = 0;}
     	 if( $tab['views']) {$vues = $tab['views'];}else{$vues = 0;}
     	 $sql = "UPDATE `InfosVideos` SET `Vues`='$vues', `Likes`='$likes', `Dislikes`='$dislikes' WHERE `IDUrl`='$idURL';"; 
		 enregistrer($sql, $HOST, $USER, $MDP, $BDD);
     }
   }
  }

$tFin=microtime(true);
$tps=$tFin-$tDeb;
$exec = number_format($tps, 3);
echo "Debut du script: ".date("H:i:s", $tDeb)."\n\r";
echo "Fin du script: ".date("H:i:s", $tFin)."\n\r";
echo "Script exécuté en " . $exec . " sec"."\n\r";
  
