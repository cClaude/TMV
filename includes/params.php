<?php
//Fichier de configuration du site
//Droits : chmod 640
$DOMAINE = "<racine du site>";
$SITE = '<chemin/du/dossier>'; //Dossier d'installation du site
$HOST = "localhost";
$USER = "<utilisateur>"; //Remplacer par l'utilisateur de la BDD
$BDD = "trouvemavideo"; //Remplacer par le nom de la base de données
$MDP = "********"; //Remplacer par le mot de passe de l'utilisateur
$LANGUE = "fr"; // Langues possibles "en" et "fr"
$ADMIN_NOM = "<nom>"; //supprimer le nom après le premier lancement
$ADMIN_MDP = "*******"; //supprimer le mot de passe après le premier lancement.