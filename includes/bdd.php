<?php
//**************************************************************************
//							OPÉRATIONS DE BASE SUR LA BASE DE DONNÉES
//**************************************************************************
//============================================================
//Se connecter à la base de données
//============================================================
function connectDB($HOST, $USER, $PASS, $BDD) {
	error_reporting(E_ALL & ~E_WARNING);
	$lien = mysqli_connect($HOST, $USER, $PASS, $BDD);
	if (!$lien) {
		debogue('bdd_erreur', 'CONNEXION', 'impossible de se connecter à MYSQL' . mysqli_connect_error());
		die('ERROR_CONNECTION_DB');
	}
	return $lien;
}
//===========================================================
//Enregistrer des informations dans la bdd
//===========================================================
function enregistrer($sql, $HOST, $USER, $PASS, $BDD) {
	$rep = 1;
	$lien = connectDB($HOST, $USER, $PASS, $BDD);
	if (!mysqli_query($lien, $sql)) {
		debogue('bdd_erreur', 'REQUÊTE', 'impossible d\'effectuer la requête' .'<br>'.$sql.'<br>'.mysqli_error($lien));
		mysqli_close($lien);
		die($rep = 0);
	}
	$rep = mysqli_insert_id($lien);
	mysqli_close($lien);
	return $rep;
}
//============================================================
//Charger une table
//============================================================
function chargerTable($sql, $HOST, $USER, $PASS, $BDD) {
	$lien = connectDB($HOST, $USER, $PASS, $BDD);
	if ($result = mysqli_query($lien, $sql)) {
		$nbrLignes = mysqli_num_rows($result);
		for ($i = 0;$i < $nbrLignes;$i++) {
			$tableau[$i] = mysqli_fetch_array($result, MYSQLI_ASSOC);
		}
		mysqli_free_result($result);
		mysqli_close($lien);
	} else {
		debogue('bdd_erreur', 'CHARGEMENT', 'impossible d\'effectuer la requête' . mysqli_error($lien));
		die($rep = 0);
		mysqli_close($lien);
	}
	if (isset($tableau)) {
		return $tableau;
	} else {
		return false;
	}
}
/*================================================================*/
/* Créer les tables de la base de données                         */
/*================================================================*/
function creerTables() 
{
	include ("params.php");
	//Comptes utilisateurs
	$sql = "CREATE TABLE IF NOT EXISTS `Admin` (";
	$sql.= "`IDAdmin` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomAdmin` VARCHAR(64) NOT NULL, ";
	$sql.= "`MDP` VARCHAR(256) NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDAdmin`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des tâches
	$sql = "CREATE TABLE IF NOT EXISTS `Taches` (";
	$sql.= "`IDTache` INT(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`Commande` TEXT NOT NULL, ";
	$sql.= "`Encours` BOOLEAN NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDTache`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des instances
	$sql = "CREATE TABLE IF NOT EXISTS `Instances` (";
	$sql.= "`IDInst` INT(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomInst` VARCHAR(64) NOT NULL, ";
	$sql.= "`URLInst` TEXT NOT NULL, ";
	$sql.= "`NbrVideo` INT UNSIGNED, ";
	$sql.= "`DescInst` TEXT NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDInst`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
	//Table des Catégories des vidéos
	$sql = "CREATE TABLE IF NOT EXISTS `CatInstance` (";
	$sql.= "`IDCat` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomCat` VARCHAR(64) NOT NULL, ";
	$sql.= "`NomCatFr` VARCHAR(64) NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDCat`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des Chaînes de vidéos
	$sql = "CREATE TABLE IF NOT EXISTS `Chaines` (";
	$sql.= "`IDChaine` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomChaine` VARCHAR(256) NOT NULL, ";
	$sql.= "`UUIDChaine` CHAR(36) NOT NULL, ";
	$sql.= "`IDInst` int(11) NOT NULL,";
	$sql.= "`URLChaine` TEXT, ";
	$sql.= "`DescChaine` TEXT NOT NULL, ";
	$sql.= "`Compte` VARCHAR(256) NOT NULL, ";
	$sql.= "`Adeptes` INT NOT NULL, ";
	$sql.= "`Favoris` INT NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDChaine`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Requêtes des utilisateurs pour l'admin
	$sql = "CREATE TABLE IF NOT EXISTS `Requetes` (";
	$sql.= "`IDRequete` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`Motif` TEXT NOT NULL,";
	$sql.= "`Email` VARCHAR(320) NOT NULL, ";
	$sql.= "PRIMARY KEY (`IDRequete`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des langues disponnibles
	$sql = "CREATE TABLE IF NOT EXISTS `Langues` (";
	$sql.= "`IDLangue` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomLangue` VARCHAR(64) NOT NULL,";
	$sql.= "`NomLangueFr` VARCHAR(64) NOT NULL,";	
	$sql.= "PRIMARY KEY (`IDLangue`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des types de licences
	$sql = "CREATE TABLE IF NOT EXISTS `Licences` (";
	$sql.= "`IDLicence` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`NomLicence` VARCHAR(256) NOT NULL,";
	$sql.= "PRIMARY KEY (`IDLicence`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
	//Table des tags
	$sql = "CREATE TABLE IF NOT EXISTS `Cles` (";
	$sql.= "`IDCle` int(11) NOT NULL, ";
	$sql.= "`NomCle` VARCHAR(64) NOT NULL,";
	$sql.= "PRIMARY KEY (`IDCle`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);		
	//URLs des Vidéos
	$sql = "CREATE TABLE IF NOT EXISTS `URLVideos` (";
	$sql.= "`IDURL` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`URL` TEXT NOT NULL, ";
	$sql.= "`IDInst` int(11) NOT NULL,";
	$sql.= "`Principal` BOOLEAN, ";
	$sql.= "PRIMARY KEY (`IDURL`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	//Table des vidéos
	$sql = "CREATE TABLE IF NOT EXISTS `InfosVideos` (";
	$sql.= "`IDVideo` int(11) NOT NULL AUTO_INCREMENT, ";
	$sql.= "`IDPT` int(11) NOT NULL,";
	$sql.= "`IDInst` int(11) NOT NULL,";
	$sql.= "`Titre` VARCHAR(256) NOT NULL, ";
	$sql.= "`UUIDVideo` CHAR(36) NOT NULL, ";
	$sql.= "`IDUrl` int(11) NOT NULL,";
	$sql.= "`creation` DATETIME NOT NULL,";
	$sql.= "`Parution` DATETIME NOT NULL,";
	$sql.= "`Actualisation` DATETIME NOT NULL,";
	$sql.= "`IDCat` int(11) NOT NULL,";
	$sql.= "`IDLangue` int(11) NOT NULL,";
	$sql.= "`IDLicence` int(11) NOT NULL,";
	$sql.= "`IDChaine` int(11) NOT NULL,";	
	$sql.= "`confiance`  VARCHAR(32) NOT NULL,"; //Public, Unlisted, Private
	$sql.= "`Description` TEXT NOT NULL,";
	$sql.= "`Duree` BIGINT NOT NULL,";	
	$sql.= "`Local` BOOLEAN NOT NULL,";	
	$sql.= "`URLPhoto` TEXT NOT NULL, ";	
	$sql.= "`Vues` INT NOT NULL,";
	$sql.= "`Likes` INT NOT NULL,";	
	$sql.= "`Dislikes` INT NOT NULL,";
	$sql.= "`SFW` INT NOT NULL,";		
	$sql.= "`tabCles` TEXT NOT NULL,";	
	$sql.= "PRIMARY KEY (`IDVideo`)";
	$sql.= ") DEFAULT CHARSET=utf8 ;";
	enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
	
}
/*================================================================*/
/* Enregistrer les infos admin si nécessaire                      */
/*================================================================*/
function creerInfosAdmin() {
	include ("params.php");
	$sql = "SELECT * FROM `Admin`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if (!$result) {
		$hash = password_hash($ADMIN_MDP, PASSWORD_BCRYPT);
		$sql = "INSERT INTO `Admin`(`NomAdmin`,`MDP`) VALUES ('$ADMIN_NOM','$hash');";
		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
		return true;
	} else {
		debogue('bdd_erreur', 'CREER_ADMIN', 'Impossible de créer le compte' . $ADMIN_NOM);
		return false;
	}
	return true;
}
/*================================================================*/
/* Tester la connexion de l'administrateur                        */
/*================================================================*/
function verifConnexion($nom, $mdp) {
	include ("params.php");
	$sql = "SELECT * FROM `Admin`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if (!strcmp($result[0]['NomAdmin'], $nom)) {
		if (password_verify($mdp, $result[0]['MDP'])) {
			return true;
		}
	}
	debogue('bdd_erreur', 'AUTH', 'Impossible de connecter ' . $ADMIN_NOM);
	return FALSE;
}
/*================================================================*/
/* Trouver le nombre de vidéo d'une instance                      */
/*================================================================*/
function nbrVideos() {
	include ("params.php");
	$sql = "SELECT SUM(`NbrVideo`) FROM `Instances`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if (!$result) {
		debogue('bdd_erreur', 'INSTANCE', 'Impossible de trouver le nombre de vidéos total ');
		return FALSE;
	}
	$nbr = $result[0]['SUM(`NbrVideo`)'];
	if(!$nbr)
	{
		$nbr=0;
	}
	return $nbr;
}
/*================================================================*/
/* Charger toutes les catégories de vidéo                         */
/*================================================================*/
function chargerCategos() {
	include ("params.php");
	if($LANGUE = 'fr') 
	{
		$sql = "SELECT `IDCat`,`NomCatFr` FROM `CatInstance` ORDER BY `NomCatFr`;";
	}else{
	   $sql = "SELECT `IDCat`,`NomCat` FROM `CatInstance` ORDER BY `NomCat`;";
	}
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'CATEGORIES', 'Impossible de charger les catégories.');
      return FALSE;	
	}
}
/*================================================================*/
/* Trouver la catégorie d'une vidéo à partir de IDCat             */
/*================================================================*/
function trouverCatego($idCat) {
	include ("params.php");
	$sql = "SELECT * FROM `CatInstance` WHERE `IDCat`='$idCat';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result[0];
	}else{
		debogue('bdd_erreur', 'CATÉGORIE', 'Impossible de charger la catégorie.');
      return FALSE;	
	}
}
/*================================================================*/
/* Charger toutes les instances Peertube                          */
/*================================================================*/
function chargerInstances() {
	include ("params.php");
	$sql = "SELECT `IDInst`,`NomInst`,`URLInst`,`NbrVideo` FROM `Instances` ORDER BY `NomInst`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'INSTANCE', 'Impossible de charger les instances.');
      return FALSE;	
	}
}
/*================================================================*/
/* Trouver une Instance à partir de son ID                        */
/*================================================================*/
function trouverInst($idInst) {
	include ("params.php");
	$sql = "SELECT * FROM `Instances` WHERE `IDInst` = '$idInst';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);	
	if($result)
	{
		return $result[0];
	}else{
		debogue('bdd_erreur', 'INSTANCE', ' L\'instance '. $idInst.' n\'est pas trouvée.');
      return FALSE;	
	}
}
/*================================================================*/
/* Trouver l'ID d'une instance Peertube à partir de son nom       */
/*================================================================*/
function trouverIDInst($instance) {
	include ("params.php");
	$instance = htmlentities($instance);
	$sql = "SELECT `IDInst` FROM `Instances` WHERE `NomInst` = '$instance';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);	
	if($result)
	{
		return $result[0]['IDInst'];
	}else{
		debogue('bdd_erreur', 'INSTANCE', ' L\'ID de l\'instance '. $instance.' n\'est pas trouvée.');
      return FALSE;	
	}
}
/*================================================================*/
/* Filtrer  les instances Peertube non connectées       		  */
/*================================================================*/
function instancesOn()
{
  $instances = chargerInstances();
  $instOn = [];
  $nbrInst = count($instances);
  //tester les urls
  for ($i=0;$i<$nbrInst ; $i++)
  {
  	 $url = "https://".$instances[$i]['URLInst'];
  	 if(urlExiste($url))
  	 {
  	 	$id = $instances[$i]['IDInst'];
  	 	array_push($instOn,$id);
  	 }
  }
  return $instOn;
}
/*================================================================*/
/* Trouver l'URL d'une vidéo à partir de IDURL                    */
/*================================================================*/
function trouverURLVideo($idURL) {
	include ("params.php");
	$sql = "SELECT * FROM `URLVideos` WHERE `IDURL`='$idURL';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result[0];//return l'id, l'URL et l'IDInst et la priorité
	}else{
		debogue('bdd_erreur', 'URL', 'URL de la vidéo n\'existe pas.'.$idURL);
      return FALSE;	
	}
}
/*================================================================*/
/* Charger toutes les chaines de vidéos                           */
/*================================================================*/
function chargerChaines() {
	include ("params.php");
	$sql = "SELECT `IDChaine`,`NomChaine`,`UUIDChaine`,`IDInst`,`URLChaine`,`DescChaine`,";
	$sql .= "`Adeptes`,`Favoris` FROM `Chaines` ORDER BY `NomChaine`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'CHAINES', 'Impossible de charger les chaînes.');
      return FALSE;	
	}
}
/*================================================================*/
/* Charger les chaines de vidéos d'une instance                   */
/*================================================================*/
function chargerChainesInst($idInst) {
	include ("params.php");
	$sql = "SELECT * FROM `Chaines` WHERE `IDInst` = '$idInst' ORDER BY `NomChaine`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'CHAINES', 'Impossible de charger les chaînes de l\'instance.'.$idInst);
      return FALSE;	
	}
}
/*================================================================*/
/* Trouver la chaîne d'une vidéo à partir de IDChaine             */
/*================================================================*/
function trouverChaine($idChaine) {
	include ("params.php");
	$sql = "SELECT * FROM `Chaines` WHERE `IDChaine`='$idChaine';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result[0];//return l'id, l'URL et l'IDInst et la priorité
	}else{
		debogue('bdd_erreur', 'CHAINE', 'Chaîne de la vidéo n\'existe pas.'.$idChaine);
      return FALSE;	
	}
}
/*================================================================*/
/* Charger toutes les langues                                     */
/*================================================================*/
function chargerLangues() {
	include ("params.php");
	$sql = "SELECT * FROM `Langues` ORDER BY `NomLangueFr`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'LANGUES', 'Impossible de charger les langues.');
      return FALSE;	
	}
}
/*================================================================*/
/* Charger toutes les licences                                    */
/*================================================================*/
function chargerLicences() {
	include ("params.php");
	$sql = "SELECT `IDLicence`,`NomLicence` FROM `Licences` ORDER BY `NomLicence`;";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if($result)
	{
		return $result;
	}else{
		debogue('bdd_erreur', 'LICENCES', 'Impossible de charger les licences.');
      return FALSE;	
	}
}
/*================================================================*/
/* Enregistrer une instance de Peertube                           */
/*================================================================*/
function saveInstance($instance, $urlInst, $nbrVideos, $description)
 {
	include ("params.php");
   $instance = addslashes(htmlentities($instance, ENT_QUOTES));
   $description =addslashes(htmlentities($description, ENT_QUOTES));

	//Trouver l'instance
		$sql = "SELECT `IDInst` FROM `Instances` WHERE `NomInst`='$instance';";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);

	if (!$result) 
	{
		//Enregistrer l'instance s'il n'existe pas
		$sql = "INSERT INTO `Instances`(`NomInst`,`URLInst`,`NbrVideo`,`DescInst`)";
		$sql.= "VALUES ('$instance','$urlInst','$nbrVideos','$description') ";
		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
		//Trouver l'id de l'hébergeur
		$sql = "SELECT `IDInst` FROM `Instances` WHERE `NomInst`='$instance';";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
		return $result[0]['IDInst'];
	}else{
	  $idInst = $result[0]['IDInst'];
     //Mise à jour de l'instance si elle existe
     $sql = "UPDATE `Instances` SET `NomInst`='$instance',`URLInst`='$urlInst',";
     $sql .= "`NbrVideo`='$nbrVideos',`DescInst`='$description' WHERE `IDInst`='$idInst';";
     enregistrer($sql, $HOST, $USER, $MDP, $BDD);
     $sql = "SELECT `IDInst` FROM `Instances` WHERE `NomInst`='$instance';";
	  $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);   
	  return $result[0]['IDInst'];
  	}	
}
/*================================================================*/
/* Enregistrer une URL de vidéo                                   */
/*================================================================*/
function saveURLVideo($url, $idInst, $rg) {
	include ("params.php");
	$idURL = 0;
	//Trouver l'id de l'URL
	$sql = "SELECT `IDURL` FROM `URLVideos` WHERE `URL`='$url';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	debogue('bdd','URL_VIDEO',$url.'**'.$result[0]['IDURL']);
	if (!$result) 
	{
		//Enregistrer l'URL de la vidéo
		$sql = "INSERT INTO `URLVideos`(`URL`,`IDInst`,`Principal`)";
		$sql.= "VALUES ('$url','$idInst','$rg') ";
		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
		//Trouver l'id de l'URL
		$sql = "SELECT `IDURL` FROM `URLVideos` WHERE `URL`='$url';";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
		$idURL = $result[0]['IDURL'];
	}
	return $idURL;
}
/*================================================================*/
/* Enregistrer une catégorie                                      */
/*================================================================*/
function saveCatego($cat) {
	include ("params.php");
	//Trouver l'id de la catégorie
	$sql = "SELECT `IDCat` FROM `CatInstance` WHERE `NomCat`='$cat';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if (!$result) 
	{
		//Enregistrer la catégorie si elle n'existe pas
		$sql = "INSERT INTO `CatInstance`(`NomCat`,`NomCatFr`)";
		$sql.= "VALUES ('$cat','$cat') ";
		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
		//Trouver l'id de la catégorie
		$sql = "SELECT `IDCat` FROM `CatInstance` WHERE `NomCat`='$cat';";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
		return $result[0]['IDCat'];
	}
	return $result[0]['IDCat'];
}
/*================================================================*/
/* Enregistrer une langue                                         */
/*================================================================*/
function saveLangue($langue)
{
  if($langue =='Unknown') return 0;
  $traducFr = [
  					'French'     => htmlentities('Français'),
  					'English'    => htmlentities('Anglais'),
  					'Spanish'    => htmlentities('Espagnol'),
  					'German'	    => htmlentities('Allemand'),
  					'Italian'    => htmlentities('Italien'), 
  					'Dutch'	    => htmlentities('Néerlandais'),
  					'Portuguese' => htmlentities('Portugais'),
  					'Polish'	 	 => htmlentities('Polonais'),
  					'Russian'	 => htmlentities('Russe'),
  					'Chinese'	 => htmlentities('Chinois'),
  					'Japanese'	 => htmlentities('Japonais'),
  					'Norwegian'	 => htmlentities('Norvégien'),
  					'Swedish'	 => htmlentities('Suédois'),
  					'Greek'	    => htmlentities('Grec'),
  					  
  				  ];
  include ("params.php");
  $langueFr =  $traducFr[$langue];
  //Trouver l'id de la langue
  $sql = "SELECT `IDLangue` FROM `Langues` WHERE `NomLangue`='$langue';";
  $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
  if (!$result) 
  {
  	 $sql = "INSERT INTO `Langues`(`NomLangue`,`NomLangueFr`) VALUES ('$langue','$langueFr');";
  	 enregistrer($sql, $HOST, $USER, $MDP, $BDD);
  	 $sql = "SELECT `IDLangue` FROM `Langues` WHERE `NomLangue`='$langue';";
  	 $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	 return $result[0]['IDLangue'];
  }
	return $result[0]['IDLangue']; 	
}
/*================================================================*/
/* Enregistrer une licence                                        */
/*================================================================*/
function saveLicence($licence)
{
	if($licence =='Unknown') return 0;
	include ("params.php");
	//Trouver l'id de la licence
	$sql = "SELECT `IDLicence` FROM `Licences` WHERE `NomLicence`='$licence';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if (!$result) 
  	{	
  		$sql = "INSERT INTO `Licences`(`NomLicence`) VALUES ('$licence');";
  		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
  		$sql = "SELECT `IDLicence` FROM `Licences` WHERE `NomLicence`='$licence';";
		$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	 	return $result[0]['IDLicence'];
 	}
	return $result[0]['IDLicence']; 	
}
/*================================================================*/
/* Enregistrer une chaîne                                         */
/*================================================================*/
function saveChaine($idInst,$nomChaine, $urlChaine, $uuidChaine,$descChaine,$compte,$favoris,$adeptes)
{
	include ("params.php");
	$nomChaine = addslashes(htmlentities($nomChaine));
	$descChaine = addslashes(htmlentities($descChaine));
	//Trouver l'id de la chaîne
	$sql = "SELECT `IDChaine` FROM `Chaines` WHERE `NomChaine`='$nomChaine';";
	$result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	if(!$result)
	{
	  $sql = "INSERT INTO `Chaines`(`NomChaine`,`UUIDChaine`,`IDInst`,`URLChaine`,";
	  $sql .="`DescChaine`,`Compte`,`Adeptes`,`Favoris`)";
	  $sql .="VALUES ('$nomChaine','$uuidChaine','$idInst','$urlChaine','$descChaine',";
	  $sql .="'$compte','$adeptes','$favoris');";
	  enregistrer($sql, $HOST, $USER, $MDP, $BDD);
	  
	  $sql = "SELECT `IDChaine` FROM `Chaines` WHERE `NomChaine`='$nomChaine';";
	  $result = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	  return $result[0]['IDChaine'];
	}else{
		$sql = "UPDATE `Chaines` SET `NomChaine`='$nomChaine',`UUIDChaine`='$uuidChaine',";
		$sql .="`IDInst`='$idInst',`URLChaine`='$urlChaine',`DescChaine`='$descChaine',";
		$sql .="`Compte`='$compte',`Adeptes`='$adeptes',`Favoris`='$favoris' WHERE `NomChaine`='$nomChaine';";
		enregistrer($sql, $HOST, $USER, $MDP, $BDD);
		return $result[0]['IDChaine'];
	}
	debogue('bdd_erreur', 'CHAINES', 'Erreur');
	return FALSE;
}
/*================================================================*/
/* Enregistrer une vidéo                                       */
/*================================================================*/
function saveVideo($idInst, $tableau,$num,$host,$total)
{
	include ("params.php");
  	if (isset($tableau['data'][$num])) 
		{
			//Le titre
			$titre = ucfirst(ltrim(strtolower($tableau['data'][$num]['name'])));
			//ID
			$idPT = $tableau['data'][$num]['id'];
			//UUID
			$uuid = $tableau['data'][$num]['uuid'];
			//URL --> à enregistrer dans la table
			$url =  'https://'.$host.'/videos/watch/'.$uuid;
			//Date de création
			$tps = $tableau['data'][$num]['createdAt'];
			$creation = date('Y-m-d H:i:s', strtotime($tps));
			//Date de publiction
			$tps = $tableau['data'][$num]['publishedAt'];
			$parution = date('Y-m-d H:i:s', strtotime($tps));
			//Date d'actualisation
			$tps = $tableau['data'][$num]['updatedAt'];
			$actu = date('Y-m-d H:i:s', strtotime($tps));
			//Catégories --> à enregistrer dans la table
			$categorie = $tableau['data'][$num]['category']['label'];
			//Langues --> à enregistrer dans la table
			$langue = $tableau['data'][$num]['language']['label'];
			//Licences --> à enregistrer dans la table
			$licence = $tableau['data'][$num]['licence']['label'];
			//Nom de la chaîne  --> à enregistrer dans la table
			$nomChaine = $tableau['data'][$num]['channel']['displayName'];
			//url de la chaine
			$urlChaine = $tableau['data'][$num]['channel']['url'];
			//uuid de la chaine
			$uuidChaine = $tableau['data'][$num]['channel']['uuid'];
			//Compte de la chaine
			$compte = $tableau['data'][$num]['account']['name'];
			//Adeptes et Favoris de la chaine
			$urlCompteAPI = 'https://' .$host. '/api/v1/accounts/'.$compte;
			$jsonCompte = file_get_contents($urlCompteAPI);
			$tabCompte = json_decode ($jsonCompte,true);
			$favoris = $tabCompte['followingCount'];
			$adeptes = $tabCompte['followersCount'];	
			if(!$adeptes) {$adeptes = 0;}
			if(!$favoris) {$favoris = 0;}
			//Confiance 
			$confiance = $tableau['data'][$num]['privacy']['label'];
			//Description
			$description = $tableau['data'][$num]['description'];
			//Durée
			$duree = $tableau['data'][$num]['duration'];
			//Local
			$local = $tableau['data'][$num]['isLocal'];
			//miniature de la vidéo
			$miniature = $tableau['data'][$num]['thumbnailPath'];
			//Vues
			$vues = $tableau['data'][$num]['views'];
			//Likes
			$likes = $tableau['data'][$num]['likes'];
			//Dislikes
			$dislikes = $tableau['data'][$num]['dislikes'];
			//Safe For Work
			$sure = $tableau['data'][$num]['nsfw'];
			if(!$sure) {$sfw = 0;} else{ $sfw = 1;}
			
			$idURL = saveURLVideo($url,$idInst,1);
			debogue('bdd', 'IDURL', $idURL."<br>"); 
			$idCat = saveCatego($categorie);
			debogue('bdd', 'IDCat', $idCat."<br>"); 
			$idLangue = saveLangue($langue);
			debogue('bdd', 'IDLangue', $idLangue."<br>"); 
			$idLicence = saveLicence($licence);
			debogue('bdd', 'IDLicence', $idLicence."<br>"); 	
			$idChaine = saveChaine($idInst,$nomChaine, $urlChaine, $uuidChaine,'',$compte,$favoris,$adeptes);
			debogue('bdd', 'IDChaine', $idChaine."<br>"); 
			$titre = addslashes(htmlentities($titre));
			$description = addslashes(htmlentities($description));
			if($idURL!=0)
			{
                 $sql =  "INSERT INTO `InfosVideos`(`IDPT`,`IDInst`,`Titre`,`UUIDVideo`,`IDUrl`,`creation`,";
                 $sql .= "`Parution`,`Actualisation`,`IDCat`,`IDLangue`,`IDLicence`,`IDChaine`,`confiance`,";
                 $sql .= "`Description`,`Duree`,`Local`,`URLPhoto`,`Vues`,`Likes`,`Dislikes`,`SFW`,`tabCles`)";
                 $sql .= "VALUES ('$idPT','$idInst','$titre','$uuid','$idURL','$creation','$parution','$actu','$idCat',";
                 $sql .= "'$idLangue','$idLicence','$idChaine','$confiance','$description','$duree','$local',";
                 $sql .= "'$miniature','$vues','$likes','$dislikes','$sfw','');";	
                 $rep = enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
			}	
			 updateNbrVideo($idInst,$total);				
		}
}
/*================================================================*/
/* Mettre à jour le nombre de vidéos d'une instance               */
/*================================================================*/
function updateNbrVideo($idInst,$total)
{
  include ("params.php");
  $sql = "UPDATE `Instances` SET `NbrVideo`='$total' WHERE `IDInst`='$idInst';";
  enregistrer($sql, $HOST, $USER, $MDP, $BDD);	
}
/*================================================================*/
/* Test et enregistrement d'une instance proposée                 */
/*================================================================*/
function gestionContact($url,$email,$captcha)
{
    include ("params.php");
    //Test captcha
    $captchaLu = lireDoc("captcha/captcha.txt",$captcha); 
    if(intval($captchaLu)!=intval($captcha))
     {
         return "Les nombres ne sont pas identiques.";   
      }	
	 //Test de l'instance 
	 if(!testInstancePeertube($url)) 
	  {
	  	 return $url." n'est pas une instance Peertube.";
	  }
	  
	 //Instance déjà référencée ?
	 $result = parse_url($url);
	 $hote = $result['host'];
	 $sql ="SELECT * FROM `Instances` WHERE `URLInst`='$hote';";
	 $reponse = chargerTable($sql, $HOST, $USER, $MDP, $BDD);
	 if($reponse)
	 {
	 	return $reponse[0]['NomInst']." est déjà référencée";
	 }

    //Enregistrer la demande
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
    	$email = "";
    }    	
    $sql = "INSERT INTO `Requetes`(`Motif`,`Email`) VALUES ('$url','$email');";	
    enregistrer($sql, $HOST, $USER, $MDP, $BDD);   
    $msg = "Merci d'avoir proposé cette nouvelle instance Peertube.<br> Si cette instance correspond aux attentes et aux capacités de TrouveMavidéo,
    elle sera référencée le plus tôt possible.";
    return $msg;
}