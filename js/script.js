/*Fonctions javascript pour TrouveMaVideo*/

function paginer(pageOld, page) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("tableau").innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/pageRecherche.ajax.php?page=" + page + "&pageOld=" + pageOld, true);
	xhttp.send();
}

function getXMLHttpRequest() {
	var xhr = null;

	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}

	return xhr;
}

function adapteInstance() {
	var instance = document.getElementById('idInstance');
	var index = instance.options.selectedIndex;
	var choix = instance.options[index].innerHTML;
   
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("idChaine").innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/chainesInstance.ajax.php?choix=" + choix, true);
	xhttp.send();
}

function majVideos()
{
	var instance = document.getElementById('idRefInst');
	var index = instance.options.selectedIndex;
	var choix = instance.options[index].innerHTML;

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("idMaj").innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/majVideos.ajax.php?choix=" + choix, true);
	xhttp.send();

}

function nbrVideosPage()
{
  var nbr = document.getElementById('nbrVideosPage').value;
  	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log(this.responseText);
			 location.reload(); 
		}
	};

	xhttp.open("GET", "ajax/nbrVideosPage.ajax.php?nbr=" + nbr, true);
	xhttp.send();
}
