********************************************************************************
*                           LICENCES -- TrouveMaVideo                          *
********************************************************************************


TrouveMaVideo : site de référencement de vidéos

------------------------------------------------------------------------
Licence du site :
Attribution - Pas d’Utilisation Commerciale 3.0 France (CC BY-NC 3.0 FR) 
https://creativecommons.org/licenses/by-nc/3.0/fr/

------------------------------------------------------------------------
API PEERTUBE  
https://docs.joinpeertube.org/api-rest-reference.html
Licence : AGPLv3.0  https://github.com/Chocobozzz/PeerTube/blob/master/LICENSE

------------------------------------------------------------------------
Feuille de style : w3.css
https://www.w3schools.com/w3css/default.asp

------------------------------------------------------------------------
Icônes des boutons

https://feathericons.com/

------------------------------------------------------------------------
Polices de caractères
