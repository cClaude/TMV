# TrouveMaVideo V0.2
Site de référencement et de recherche par critère de vidéos hébergées sur diverses instances de Peertube.
[Site de démonstration](https://cogito.no-ip.info/cogito/TMV/index.php)

## Organisation générale :

### sur le client 6 types d'action :
* l'utilisateur fait une recherche de vidéo
* se connecter en tant qu'administrateur
* envoyer une demande (référencement d'instance) à l'administrateur.
* utiliser l'aide de l'application,
* consulter les informations légales du site.

### côté serveur :
* le serveur va chercher les informations sur le site de la vidéo et récupérer
les informations entrées par l'utilisateur.
* le serveur va répondre à une recherche de vidéo.

### Sites hébergeurs de vidéos :  
fournissent le titre, la date de publication ,le nom de la chaîne, 
éventuellement des catégories et d'autres informations.

## Installation sur un serveur Linux:
   1. Créer une base de données 
   2. Installer git et les modules GD et CURL de PHP.
   3. Depuis le dossier du site lancer la commande :
   
`git clone https://framagit.org/acryline/TMV.git`

   4. Ensuite pour chaque mise à jour

`git pull https://framagit.org/acryline/TMV.git`
    
   5. Configurer le fichier de configuration params.php 
   6. Donner les droits d'écriture sur les dossiers log et captcha
   (chmod -R 777)
   7. Droits sur le fichier refVideo.php, majTMV.php et majLikesVues.php : 
   chmod 754 (exécutable) et propriétaire www-data. 
   8. Au premier lancement, dans index.php, décommenter les deux lignes 
   
`//creerTables();`

`//creerInfosAdmin

Après le premier lancement, recommenter les deux  lignes et supprimer 
le nom de l'admin et son mot de passe dans param.php

   9. Mise à jour automatique via Cron de l'utilisateur www-data.

`crontab -e -u www-data`    

`*/30 * * * *   <chemin installation>/trouvermavideo/includes/majTMV.php

`* 11 * * *     <chemin installation>/trouvermavideo/includes/majLikesVues.php >>  <chemin>/log/majLDV.log

Changer les périodes de lancement des scripts à votre convenance. Par exemple ici :
- Premier script : toutes les 30 minutes 
- Deuxième script (opération assez longue) : tous les jours à 11h
## Prochaines implémentations :
- Suppression d'un référencement (vidéo qui n'existe plus)
- Mise à jour des vidéos enregistrées
- Nouveau thème
- Système d'installation facile

Ce site est mis à disposition sans aucune garantie. Vous pouvez utiliser ce 
code à vos risques et périls. Vous êtes entièrement responsable de ce 
vous en faites et des conséquences que cela implique.

